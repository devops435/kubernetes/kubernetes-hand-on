# Run in Locally with `docker-compose`

1. Go to employee microserivcce `$ cd employee`
2. > $ ./gradlew build
   >
3. > $ docker-compose up --build -d
   >
4. > $ curl localhost:8080/employee/test/hello
   >

`<br>` `<br>`

# Minikube Kubernetes

[Push Registry Guide](Capture.PNG)

1. push `employee` image to registry
   - > $ docker-compose up --build -d
     >
   - > docker tag service-employee:latest bunthai/service-employee:latest
     >
   - > docker push bunthai/service-employee:latest
     >
2. Add `127.0.0.1 employee.com ` -> `hosts` file
3. Enable Ingression
   - With Minikube: `$ minikube addons enable ingress`
   - With docker desktop: `$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.7.0/deploy/static/provider/cloud/deploy.yaml`
4. `$ kubectl apply -f posgresql-deployment.yaml`
5. `$ kubctl apply -f posgresql-service.yaml`
6. `$ kubectl apply -f a-employee-deployment.yaml`
7. `$ kubectl apply -f b-employee-service.yaml`
8. `$ kubectl apply -f c-employee-ingression.yaml`
9. `$ minikube.exe tunnel`

# To expose service access from outside

1. `$ kubectl apply -f employee-service.yaml`
2. `$ minikube service employee-service.yaml`
3. access http://127.0.0.1:{port}/employee/api/hello
